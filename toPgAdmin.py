import pandas as pd
import sqlalchemy
import openpyxl
import psycopg2

# nom des fichiers en entrée
fichier_raw = "/Users/Timy/OneDrive/Bureau/PDecisionnel/DataFiles/RAW_SLA_ARLACAN.xlsx"
fichier_ageQ = "/Users/Timy/OneDrive/Bureau/PDecisionnel/DataFiles/AgeGroupeQuinquenal.csv"
fichierPoids = "/Users/Timy/OneDrive/Bureau/PDecisionnel/DataFiles/Groupe_Poids.csv"
fichierScore = "/Users/Timy/OneDrive/Bureau/PDecisionnel/DataFiles/Groupe_Score_ALS_FRS-R.csv"
fichierInsee = "/Users/Timy/OneDrive/Bureau/PDecisionnel/DataFiles/insee.xlsx"
fichierageD = "/Users/Timy/OneDrive/Bureau/PDecisionnel/DataFiles/AgeGroupDecennal.csv"
fichierMutations = "/Users/Timy/OneDrive/Bureau/PDecisionnel/DataFiles/mutationsGeneral.csv"
fichierMolecules = "/Users/Timy/OneDrive/Bureau/PDecisionnel/DataFiles/molecules.xlsx"

# Charge les fichiers dans un DataFrame
data = pd.read_excel(fichier_raw)
dataInsee = pd.read_excel(fichierInsee)
dataAgeQ = pd.read_csv(fichier_ageQ, sep=',', low_memory=False)
dataPoids = pd.read_csv(fichierPoids, sep=';', low_memory=False)
dataScore = pd.read_csv(fichierScore, sep=';', low_memory=False)
dataAgeD = pd.read_csv(fichierageD, sep=',', low_memory=False)
dataMutations = pd.read_csv(fichierMutations,sep=',', low_memory=False)
dataMolecules = pd.read_excel(fichierMolecules)

# Specification dees colonnes
colonne_debut = ['Anonymisation']
colonnes_plage1 = [data.columns[5]] + data.columns[7:23].tolist()
colonnes_plage3 = data.columns[538:673].tolist()

colonnes_fichier1 = ['Anonymisation', 'Sexe : [ SEX ] [M0 - F0]', 'Date de naissance : [ DOB ] [M0 - F0]',
                     'Activité actuelle : [ ACTPROF ] [M0 - F0]', 'Profession précédente (- 1) : [ PROF1 ] [M0 - F0]',
                     'Poids de référence (kg) : [ WEIGHT_REF ] [M0 - F0]',
                     'Poids à la première visite (kg) : [ WEIGHT ] [M0 - F0]']

colonnes_fichier2 = colonne_debut + colonnes_plage1
colonnes_fichier4 = colonne_debut + colonnes_plage3

# Sélectionne les colonnes et supprime les duplicats
#recuperation des colonnes relatives aux informations general du patient
df_fichierInfosPatients= data[colonnes_fichier1].drop_duplicates(subset=colonnes_fichier1)

#recuperation des colonnes relatives aux informations de diagnostic
df_fichierDiagnostic = data[colonnes_fichier2].drop_duplicates(subset=colonnes_fichier2)

#recuperation des colonnes relatives aux informations biologiques
df_fichierBiologiques = data[colonnes_fichier4].drop_duplicates(subset=colonnes_fichier4)

# Gestion de la table Examens :
# Extraction des codes d'examens
colonnes_codes_examens = [f"[M{i} - F0]" for i in range(35)]

# Liste vide pour stocker les nouvelles colonnes
donnees_finales = []

# Pour chaque examen, nous recuperons les informations 
for index, row in data.iterrows():
    code_patient = row["Anonymisation"]
    for code in colonnes_codes_examens:
        date_exam = row.get(f"Date de l'examen : [ DATEXAM_V ] {code}", None)
        imc = row.get(f"IMC : [ BMI_V ] {code}", None)
        poids = row.get(f"Poids du jour (kg) : [ WEIGHT_V ] {code}", None)
        score_alsfrsR = row.get(f"Score ALS FRS-R : [ ALS_V ] {code}", None)
        si_Décédé = row.get(f"Décédé ? [ DCD_V ] {code}", None)
        date_deces = row.get(f"Date du décès : [ DATEDCD_V ] {code}", None)
        cause_deces = row.get(f"Cause du décès : [ CAUSE_DCD_V ] {code}", None)
        cause_liee_sla = row.get(f"Cause liée à la SLA  : [ CAUSDCD_SLA_V ] {code}", None)
        detail_cause_deces = row.get(f"Détail de la cause du décès : [ DET_DCD_V ] {code}", None)
        autre_cause_deces = row.get(f"Si autre cause, préciser : [ AUTRE_CAUSEDCD_V ] {code}", None)
        date_premier_diagnostic = row.get(f"Date du diagnostic : [ DATEDIAG_V ] {code}", None)
        nouveau_diagnostic = row.get(f"Nouveau diagnostic : [ NEW_DIAG_V ] {code}", None)
        date_nouveau_diagnostic = row.get(f"Date du nouveau diagnostic : [ NEWDATEDIAG_V ] {code}", None)
        probabilite_diagnostic = row.get(f"Probabilité du diagnostic : [ DIAGPROBA_V ] {code}", None)

        donnees_finales.append((
            code_patient, code, date_exam, imc, poids, score_alsfrsR,
            si_Décédé, date_deces, cause_deces, cause_liee_sla,
            detail_cause_deces, autre_cause_deces, date_premier_diagnostic,
            nouveau_diagnostic, date_nouveau_diagnostic, probabilite_diagnostic
        ))

# Notre nouvelle dataFrame
df_fichierExamens = pd.DataFrame(
    donnees_finales,
    columns=[
        "Code_Patient",
        "Code_Examen",
        "Date",
        "IMC",
        "Poids",
        "Score_ALS FRS-R",
        "Si_Décédé",
        "Date_du_Décès",
        "Cause_du_Décès",
        "Cause_Liée_à_la SLA",
        "Détail_de_la_Cause_du_Décès",
        "Autre_Cause_de Décès",
        "Date_du_Premier_Diagnostic",
        "Nouveau_Diagnostic",
        "Date_du_Nouveau_Diagnostic",
        "Probabilité_du_Diagnostic"
    ]
)
df_fichierExamens = df_fichierExamens.drop_duplicates();

# Supression des espaces et caracteres speciaux pour les noms de colonnes

df_fichierInfosPatients.columns = df_fichierInfosPatients.columns.str.replace(' ', '_').str.replace(':', '_').str.replace('\[|\]','_').str.replace('-','_')
df_fichierDiagnostic.columns = df_fichierDiagnostic.columns.str.replace(' ', '_').str.replace(':', '_').str.replace('\[|\]','_').str.replace('-','_')
df_fichierBiologiques.columns = df_fichierBiologiques.columns.str.replace(' ', '_').str.replace(':', '_').str.replace('\[|\]','_').str.replace('-','_')

# connexion à la base de données PostgreSQL
user = 'postgres'
password = 'root'
host = '127.0.0.1'
database = 'my_db'
conn_str = f'postgresql+psycopg2://{user}:{password}@{host}/{database}'
engine = sqlalchemy.create_engine(conn_str)

# Gestion des types avant stockage
dtype_dict1 = {col: sqlalchemy.types.Integer if col == 'Anonymisation' else sqlalchemy.types.VARCHAR(1000) for col in
               df_fichierInfosPatients.columns}
dtype_dict2 = {col: sqlalchemy.types.Integer if col == 'Anonymisation' else sqlalchemy.types.VARCHAR(1000) for col in
               df_fichierDiagnostic.columns}
dtype_dict4 = {col: sqlalchemy.types.Integer if col == 'Anonymisation' else sqlalchemy.types.VARCHAR(1000) for col in
               df_fichierBiologiques.columns}
dtype_dict5 = {col: sqlalchemy.types.Integer if col == 'Code_Patient' else sqlalchemy.types.VARCHAR(1000) for col in
               df_fichierExamens.columns}

# Sélection des colonnes à vérifier pour la présence de valeurs non nulles
cols_to_check = df_fichierExamens.columns[2:]  # Sélection des colonnes à vérifier
# Filtre les lignes où toutes les colonnes après 'Code_Patient' et 'Code_Examen' sont vides c'est a dire que le patient n'a pas eu d'examens
df_fichierExamens_filtered = df_fichierExamens.dropna(subset=cols_to_check, how='all')

# Insertion des DataFrames dans la base de données PostgreSQL
df_fichierInfosPatients.to_sql('informations_patients', engine, if_exists='replace', index=False, dtype=dtype_dict1)
df_fichierDiagnostic.to_sql('diagnostic', engine, if_exists='replace', index=False, dtype=dtype_dict2)
df_fichierBiologiques.to_sql('analyses_biologiques', engine, if_exists='replace', index=False, dtype=dtype_dict4)
df_fichierExamens_filtered.to_sql('examens', engine, if_exists='replace', index=False, dtype=dtype_dict5)
dataScore.to_sql("Groupe_Score", engine, if_exists='replace', index=False)
dataPoids.to_sql("Groupe_Poids", engine, if_exists='replace', index=False)
dataAgeD.to_sql("AgeDecennal", engine, if_exists='replace', index=False)
dataAgeQ.to_sql("AgeQuinquennal", engine, if_exists='replace', index=False)
dataInsee.to_sql("Insee_csp", engine, if_exists='replace', index=False)
dataMutations.to_sql("MapMutations", engine,if_exists='replace', index=False)
dataMolecules.to_sql("Seuil",engine,if_exists='replace',index=False)
